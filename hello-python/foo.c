#define Py_LIMITED_API
#include <Python.h>

static PyObject* foo_sum(PyObject *self, PyObject *args);

static PyMethodDef FooMethods[] = {
    {"sum", foo_sum, METH_VARARGS, "Add to numbers."},
    {NULL, NULL, 0, NULL} /* Sentinel */
};

static PyModuleDef foomodule = {
    PyModuleDef_HEAD_INIT,
    "foo",              /* name of module */
    "Simple module",    /* module documentation */
    -1,                 /* irrelevant */
    FooMethods
};

// extern "C" PyObject* PyInit_foo()
PyMODINIT_FUNC PyInit_foo()
{
    return PyModule_Create(&foomodule);
}

static PyObject* foo_sum(PyObject *self, PyObject *args)
{
    int a, b;
    if (!PyArg_ParseTuple(args, "ii", &a, &b)) {
        return NULL;
    }

    return PyLong_FromLong(a + b);
}
