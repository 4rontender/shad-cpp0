#define Py_LIMITED_API
#include <Python.h>

static PyObject* showerr_do(PyObject *self, PyObject *args);

static PyMethodDef ShowErrMethods[] = {
    {"do", showerr_do, METH_VARARGS, "Raise error."},
    {NULL, NULL, 0, NULL} /* Sentinel */
};

static PyModuleDef showerrmodule = {
    PyModuleDef_HEAD_INIT,
    "showerr",
    "Example for exceptions",
    -1,
    ShowErrMethods
};

PyMODINIT_FUNC PyInit_showerr()
{
    return PyModule_Create(&showerrmodule);
}

static PyObject* showerr_do(PyObject *self, PyObject *args)
{
    PyErr_SetString(PyExc_RuntimeError, "My example error");
    return NULL;
}
