# Управление памятью, умные указатели, RAII

---

# У нас есть `new/delete`. В чем проблема?

```c++
void f(size_t size) {
    int *data = new int[size];

    ...

    delete[] data;
}
```

---

# Можно забыть сделать delete

У функции не всегда один return

```c++
void f(size_t size) {
    int *data = new int[size];
    ...

    if (condition1) {
        ...
        delete[] data;
        return;
    }

    if (condition2) {
        ...
        delete[] data;
        return;
    }
    delete[] data;
}
```

---

# Решение из мира C

```c++
void f(size_t size) {
    int *data = malloc(size * sizeof(int));
    ...
    if (condition) {
        ...
        goto cleanup;
    }

    if (condition2) {
        ...
        goto cleanup;
    }

    ...

cleanup:
    free(data);
}
```

---

# Обработка ошибок

```c++
void f(size_t size) {
    int *data = new int[size];

    g(); // бросает исключение, память утекла

    delete[] data;
}
```

---

# Проблема не только с памятью

```c++
void f(const char *filename) {
    FILE *fout = fopen(filename, "w");

    ...

    fclose(fout);
}
```

---

# Проблема не только с памятью

```c++
class SomeClass {
public:
    void Action() {
        mutex_.lock();

        ... // произошел досрочный выход

        mutex_.unlock();
    }

private:
    std::mutex mutex_;
};
```

---

# В общем виде

* Получаем ресурс
* Функция с ним работает
* Нужно как-то гарантировать возвращение ресурса вне зависимости от того, что происходит в функции
* Есть ли в языке такой механизм?

---

# Деструктор!

```c++
class IfStream {
public:
    IfStream(const char *filename) {
        fin_ = fopen(filename, "r");
    }

    ~IfStream() {
        fclose(fin_);
    }

private:
    FILE *fin_;
};
```

---

# RAII

* Эта идиома называется RAII (Resource Acquisition Is Initialization)
* В конструкторе получаем ресурс, деструктор его освобождает
* В какой-то степени `std::vector` это тоже RAII-обертка
* `std::ifstream`, `std::lock_guard` и много других.

```c++
void f(const char *filename) {
    IfStream fin(filename);

    // работаем с fin
}
```

---

# Проблемы с сырыми указателями

```c++
File *OpenUrl(const std::string& url) {
    return new File(url);
}

void f(const std::string& url) {
    File *document = OpenUrl(url);
    ...
    delete document;
}
```

Снова та же история. Хочется иметь такой указатель, который автоматически бы удалял уже ненужный объект.

---

# Первый шаг

```c++
class SmartPtr {
public:
    SmartPtr(File *pointer) {
        pointer_ = pointer;
    }

    ~SmartPtr() {
        delete pointer_;
    }

    File &operator*() {
        return *pointer_;
    }

    File *operator->() {
        return pointer_;
    }
private:
    File *pointer_;
};
```

---

# Используем наш "умный" указатель

```c++

SmartPtr OpenUrl(const std::string& url) {
    return SmartPtr(new File(url));
}

void f(const std::string& url) {
    SmartPtr document = OpenUrl(url);
    ...
}
```

---

# Владение ресурсом

```c++
SmartPtr a = SmartPtr(new File(...));
SmartPtr b(a); // как это должно работать?
std::vector<SmartPtr> files;
files.push_back(a);
```

Нам нужно определиться с **семантикой владения**.

---

# Единоличное владение

* Пусть есть ресурс `p`, и мы положили его в `SmartPtr a`. Тогда `p` доступен **только** через `a`, или, по-другому, `a` **владеет** ресурсом `p`.
* Это означает, что `SmartPtr b = a` запрещено

```c++
class SmartPtr {
    SmartPtr(const SmartPtr&) = delete;
    SmartPtr& operator=(const SmartPtr&) = delete;
};
```

---

# Единоличное владение

* Как же тогда возвращать `SmartPtr` из функций?
* Реализуем **передачу владения**
* Для этого в C++ есть перемещение

```c++
class SmartPtr {
    SmartPtr(SmartPtr&&) = default;
    SmartPtr& operator=(SmartPtr&&) = default;
};
```

---

# Единоличное владение

```c++

SmartPtr OpenUrl(const std::string& url) {
    return SmartPtr(new File(url));
}

void f(const std::string& url) {
    // передаем владение ресурсом (new File)
    // из функции OpenUrl в document
    SmartPtr document = OpenUrl(url);
    ...

    // SmartPtr b = document; так сделать не можем
    SmartPtr b = std::move(document);
    // а вот это ок
    // теперь b владеет ресурсом и отвечает за
    // его освобождение
}
```

---

# std::unique_ptr

Ровно эта семантика реализована в `std::unique_ptr`

```c++
#include <memory>

std::unique_ptr<File> OpenUrl(const std::string& url) {
    return std::make_unique<File>(url);
}
```

---

# Использование

* `unique_ptr` можно вернуть из функции
* Хранить в векторе (или другом контейнере). В этом случае вектор владеет всеми хранимыми объектами (dungeon).
* Его не передают в функции

```c++

void f_bad(std::unique_ptr<File> ptr) { // wtf
}

void f_ok(File *ptr) {
}

void g() {
    std::unique_ptr<File> ptr = OpenUrl(...);
    f_bad(ptr); // не скомпилируется
    f_ok(ptr.get()); // не передаем владение
}
```

---

# Разделяемое владение

Другая семантика

```c++
void f() {
    SomeSmartPtr a = SomeSmartPtr(new DbConnection(...));
    SomeSmartPtr b = a;
    ...
    // нужно закрыть соединение
}
```

---

# std::shared_ptr

* Реализует совместное владение
* Пусть есть ресурс `p` и shared_ptrы `a1`, `a2`... на него
* `p` будет освобожден только тогда, когда удалится последний `shared_ptr` на него

```c++
void g(std::shared_ptr<int> ptr) {
}

void f() {
    auto a = std::make_shared<int>(5);
    {
        auto b = a; // 2 ссылки
        g(b); // во время вызова 3 ссылки
        // после вызова снова 2
    }
    // b удалился, 1 ссылка, строка все еще не удалена
} // теперь удалена
```

---

# Итого

* Вместо `new[]` --- `std::vector`
* Вместо `new` --- `std::make_unique`
* `std::shared_ptr`
* `std::weak_ptr`