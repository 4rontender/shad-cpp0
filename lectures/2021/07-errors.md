# Обработка ошибок и исключения

Лев Высоцкий

---

# План

 * Флаги и коды ошибок
 * Either/Result
 * Исключения
 * Гарантии безопасности исключений

---
# Обработка ошибок

```c++
int main() {
  auto data = Load();
  Process(&data);
  Save(data);
  return 0;
}
```

 * Что будет, если `Load()` не сможет прочитать ввода?
 * Что будет, если `Process()` не сможет обработать данные?
 * Что будет, если `Save()` не сможет сохранить вывод?

---

# std::abort

 * В случае ошибки можно просто немедленно убивать процесс
 * `std::abort()`

---

# Флаг успешного выполнения

Будем возвращать из функции `false`, если в процессе работы произошла ошибка.

```c++
std::vector<std::string> Load();
bool Load(std::vector<std::string>* data);
std::tuple<bool, std::string> Load();

void Process(std::vector<std::string>* data);
bool Process(std::vector<std::string>* data);

void Save(const std::vector<std::string>& data);
bool Save(const std::vector<std::string>& data);
```

---
# Флаг успешного выполнения. Проблемы

 * Какие проблемы есть в этом коде? (3-4 штуки)
```c++
int main() {
  std::vector<std::string> data;
  if (!Load(&data)) {
    std::cerr << "Load failed" << std::endl;
    return 1;
  }
  if (!Process(&data)) {
    std::cerr << "Process failed" << std::endl;
  }
  Save(data);
  return 0;
}
```

---

# Флаг успешного выполнения. Проблемы

```c++
int main() {
  std::vector<std::string> data;
  if (!Load(&data)) {
    // Failed, but why?!
    std::cerr << "Load failed" << std::endl;
    return 1;
  }
  if (!Process(&data)) {
    // Failed, but why?!
    std::cerr << "Process failed" << std::endl;
    // BUG
  }
  Save(data); // BUG
  return 0;
}
```

---

# Коды возврата

 * Возвращаем код ошибки (системные вызовы linux)
 * Можно выставлять глобальную переменную (`errno`)
 * По-прежнему мало информации!

---

# Either/Result
 * Шаблон вида `Result<T,E>`
 * Конструируется либо из `T`, либо из `E` (аналогично `std::variant`)

```c++
Result<int, Error> ReadInt() {
  int i;
  std::cin >> i;
  if (std::cin.bad())
    return Error("I/O error while reading");
  else if (std::cin.eof())
    return Error("End of file");
  else if (std::cin.fail())
    return Error("Non-integer data encountered");
  return i;
}
```

---

# Either/Result
 * Шаблон вида `Result<T,E>`
 * Конструируется либо из `T`, либо из `E` (аналогично `std::variant`)
 * Методы для проверки

```c++
int main() {
  auto result = ReadInt();
  if (result.IsOk()) {
    std::cout << "Read int: " << result.Value() << std::endl;
  } else {
    std::cout << "Error: " << result.Error().Message() << std::endl;
  }
}
```
---

# Стек вызовов

```c++

void g(const std::string& x) {
  std::cout << x;
}

void f() {
  std::string x;
  std::cin >> x;
  g(x);
}

int main() {
  f();
}
```

---
# Исключения

```c++
void FailngFunc() {
  std::runtime_error error{"Fail"};
  throw error;
}

void SomeFunc() {
  FailingFunc();
}

int main() {
  try {
    SomeFunc();
  } catch (const std::exception& ex) {
    std::cerr << ex.what() << std::endl;
    return 1;
  }
  return 0;
}
```

---
# Исключения. Принцип работы


1. Функция бросает исключение.
2. Runtime находит подходящий `catch` блок.
3. Runtime "раскручивает стек", вызывая деструкторы всех локальных переменных.
4. Управление передаётся в `catch` блок.

---
# Исключения

* `SomeFunc` ничего не знает про возникновение и обработку ошибки

```c++
void FailngFunc() {
  std::runtime_error error{"Fail"};
  throw error;
}
void SomeFunc() {
  SomeOtherFunc1();
  FailingFunc();
}
int main() {
  try {
    SomeFunc();
  } catch (const std::exception& ex) {
    std::cerr << ex.what() << std::endl;
    return 1;
  }
  return 0;
}
```

---
# Исключения. Проблемы

 * Где баг?

```c++
void SomeFunc() {
  auto ptr = new Object();
  SomeOtherFunc();
  delete ptr;
}
```

---
# Исключения. Проблемы

 * Где баг?

```c++
void SomeFunc() {
  auto ptr = new Object();
  FailingFunc();
  delete ptr;
}
```

---
# Утечка памяти (memory leak)

```c++
void FailngFunc() {
  std::runtime_error error{"Fail"};
  throw error;
}
void SomeFunc() {
  auto ptr = new Object();
  FailingFunc();
  delete ptr;
}
int main() {
  try {
    SomeFunc();
  } catch (const std::exception& ex) {
    std::cerr << ex.what() << std::endl;
    return 1;
  }
  return 0;
}
```

---
# Утечка памяти (memory leak)

Чем отличаются 2 фрагмента кода?

```c++
void SomeFunc() {
  auto ptr = new Object();
  FailingFunc();
  OtherCode();
  delete ptr;
}
```

```c++
void SomeFunc() {
  std::vector<int> x(10);
  FailingFunc();
}
```


---
# Утечка памяти (memory leak)

```c++
void SomeFunc() {
  auto ptr = new Object();
  FailingFunc();
  delete ptr;
}
```

```c++
void SomeFunc() {
  auto ptr = std::make_unique<Object>();
  FailingFunc();
}
```

---
# Иерархия исключений

```c++
namespace std {
class exception {
    virtual const char* what() = 0;
};

class runtime_error : public exception { /* ... */ };
class logic_error : public exception { /* ... */ };
class invalid_argument : public logic_error {};
} // namespace std
```

---
# Иерархия исключений

```c++
void F() {
  try {
    throw std::runtime_error("FOO");
  } catch (const std::logic_error& ) {

  } catch (const std::runtime_error& ) {
    // Land here <---
  } catch (const std::exception& ) {

  }
}
```

---
# Иерархия исключений

```c++
class Error {};
class FileNotFound : public Error {};

void F() {
  try {
    File f("my.txt");
    f.Write("foo");
  } catch (const FileNotFound&) {
    CreateFile("my.txt");
    File f("my.txt");
    f.Write("foo");
  } catch (const Error&) {
    cerr << "Everything is bad :(" << std::endl;
    throw;
  }
}
```

---
# Преимущества исключений

 * Ошибка может произойти почти в каждой строчке кода
   - `IntToString("abc")`
   - `json["key"]`
   - `file.open()`
 * Многие ошибки не фатальны и не должны приводить к отказу всего процесса
   - Python REPL не смог распарсить строчку кода
   - сервер не смог обработать запрос
 * Ошибки происходят "глубоко" в стеке вызовов, а обрабатываются "наверху"
   - REPL печатает ошибку пользователю
   - сервер отвечает 500 и логирует ошибку


---
# Баги при ручной обработке ошибок

Код обработки ошибок исполняется редко и почти никогда не тестируется. Можно

 * Забыть обработать ошибку
 * Забыть освободить память из за раннего return
 * Потерять контекст ошибки

---
# Гарантии безопасности исключений

 * Любая строка кода может бросить ошибку.

```c++
void F() {
  std::vector<int> v;
  try {
    v.push_back(10);
  } catch (const std::exception& ex) {
    // ???
  }
}
```

---

# Гарантии безопасности исключений

 * **Отсутствие гарантий**
 * **Базовая гарантия**: объекты остаются в некотором корректном состоянии, ресурсы не утекают
 * **Сильная гарантия**: состояние объекта остаётся неизменным
 * **Гарантия отсутствия исключений (nothrow)**: функция никогда не бросает исключения

---

# Пример

[en.cppreference.com/w/cpp/container/vector/push_back](https://en.cppreference.com/w/cpp/container/vector/push_back)


> Exceptions
>
> If an exception is thrown (which can be due to Allocator::allocate() or element copy/move constructor/assignment), this function has no effect (**strong exception guarantee**).

