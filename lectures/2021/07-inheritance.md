---
marp: false
---

# Лекция 7. Наследование и полиморфизм

Лев Высоцкий

---

1. Имена и области видимости
2. Наследование
3. Полиморфизм
4. Явные преобразования типов

---

# Имена. Объявление vs определение

```c++
int x; // объявление + определение

void f(int y); // объявление

class A; // объявление
```

```c++
struct B { // объявление + определение
    int g(int z); // объявление
};

void f(int y) { // определение
    std::cout << y;
}
```

---

# Области видимости (scopes)

  * Глобальная
  * Блок (между `{` и `}`)
  * Тело класса/структуры
  * Пространство имён (`namespace`)

```c++
namespace std {
    ostream cout;
}
```

---

# Поиск имени (name lookup)

```c++
void f() {
    int x = 10;
    std::cout << x;
}
```

 * Имя без `::` перед ним (`std`, `x`) — unqualified lookup
 * Имя с `::` — qualified lookup

---

# Unqualified name lookup

  * Идём по вложенным областям видимости к самому внешнему

```c++
auto x = "global";
namespace N {
    auto x = "namespace";

    void f() {
        std::cout << x << std::endl;
        auto x = "function";
        std::cout << x << std::endl;
        {
            std::string x = "block";
            std::cout << x << std::endl;
        }
    }
}
int main() {
    std::cout << x << std::endl;
    N::f();
}
```

---

# Qualified name lookup

```c++
namespace N {
    struct A {
        struct B {
            const static int x = 10;
        };
    };
}
int main() {
    std::cout << N::A::B::x << std::endl;
}
```

---

# Наследование (inheritance)

```c++
class Unit {
public:
    Unit(Position pos);
    Position GetPosition() const;
private:
    Position position_;
};
```

```c++
class FightingUnit : public Unit {
public:
    FightingUnit(Position pos, double damage)
        : Unit(pos), damage_(damage) {}
    double GetDamage() const;
private:
    double damage_;
};
```

---

# Наследование

  * Публичное наследование реализует отношение "является" ("is-a")
  * С объектом-наследником можно работать как с объектом-предком

```c++
Position pos(3, 3);
Unit u(pos);
std::cout << u.GetPosition() << std::endl;

FightingUnit fu(pos, 10.0);
std::cout << fu.GetPosition() << ' ' << fu.GetDamage() << std::endl;
```

---

# Приведение указателей

```c++
void LogPosition(const Unit* unit);
void LogDamage(const FightingUnit* fu);
```

```c++
Unit u(...);
FightingUnit fu(...);

LogPosition(&u);
LogPosition(&fu);

LogDamage(&u); // не скомпилируется
LogDamage(&fu);
```

---

# Принцип подстановки Лисков

### Код, корректно работающий с объектами базового класса, должен корректно работать с объектами производного класса.

**Как не надо делать**
```c++
class Rectangle {
public:
    Rectange(int w, int h);
    void SetWidth(int);
    void SetHeight(int);
};

class Square : public Rectangle {
public:
    Square(int a) : Rectangle(a, a) {}
};
```

---

# Модификаторы доступа

```c++
class A {
public:
    char x;   // доступно всем
protected:
    char y;   // только наследникам
private:
    char z;   // только методам класса (и друзьям)
};
```

---

# Семантика наследования

 * `public` — "является" (наследование интерфейса)
 * `private` — наследование реализации (лучше использовать композицию)
 * `protected` — практически не используется

```c++
class FancyVector : private std::vector<int> {
public:
    ...
    void PushBackTwo(int x, int y) {
        push_back(x);
        push_back(y);
    }
};
```

---

# Полиморфизм

Хочется писать код, который сможет работать с объектами разных типов

```c++
template <class IntContainer>
int Sum(const IntContainer& c) {
    int sum = 0;
    for (auto x : c) {
        sum += x;
    }
    return sum;
}
```

---

# Динамический полиморфизм

 * Объявляем функцию `virtual` в базовом классе
  (чисто виртуальной, если нет разумной реализации)
 * Перегружаем (`override`) в производных
 * Работаем с объектами через (умные) указатели на базовый класс
 * Не забываем про виртуальный деструктор

---

# Приведение типов

 * `static_cast`
 * `dynamic_cast`
 * `const_cast`
 * `reinterpret_cast`

---

# Под капотом наследования

 * Базовый класс лежит в памяти в начале производного
 * Классы с виртуальными функциями хранят vptr — указатель на таблицу виртуальных функций (vtbl)
 * `dynamic_cast` работает с использование vptr

---

# Множественное наследование

```c++
class Clickable {...};
class Rectangle {...};

class Button : public Clickable, public Rectangle {...};
```