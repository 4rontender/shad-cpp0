#include <testing.h>
#include <util.h>
#include <polynom.h>
#include <vector>
#include <algorithm>
#include <cmath>
#include <iomanip>

namespace tests {

void CheckResult(const std::vector<double>& expected, const std::vector<double>& check) {
    const double eps = 1e-6;
    ASSERT_EQ(expected.size(), check.size());
    for (size_t i = 0; i < expected.size(); ++i)
        ASSERT_FLOAT_EQ(expected[i], check[i], eps);
}

void Simple() {
    {
        std::vector<double> expected{-2.3498288, 1.10086298};
        auto result = FindRoots({5, 7, -4, 11, -10, 1, -18});
        CheckResult(expected, result);
    }
}

void SomeTests() {
    {
        std::vector<double> expected{-1.0, 1.0};
        auto result = FindRoots({1, 0, 0, 0, 0, 0, -1});
        CheckResult(expected, result);
    }
    {
        std::vector<double> expected{-0.36398016, 0.0, 0.366351226, 1.93389010};
        auto result = FindRoots({2, 0, 0, -15, 0, 2, 0});
        CheckResult(expected, result);
    }
    {
        std::vector<double> expected{-9.0, -9.0, -1.5, -1.5, 3.0, 3.0};
        auto result = FindRoots({1.0, 15.0, 20.25, -351.0, -283.5, 1458.0, 1640.25});
        CheckResult(expected, result);
    }
    {
        std::vector<double> expected{3.0, 3.0, 3.0, 3.0, 3.0, 3.0};
        auto result = FindRoots({1.0, -18.0, 135.0, -540.0, 1215.0, -1458.0, 729.0});
        CheckResult(expected, result);
    }
    {
        std::vector<double> expected{-3.7, 0.0, 0.0, 0.0, 0.0, 42.42};
        auto result = FindRoots({1.0, -38.72, -156.954, 0.0, 0.0, 0.0, 0.0});
        CheckResult(expected, result);
    }
}

void AnotherTests() {
    {
        std::vector<double> expected{-1.318452, 0.538356};
        auto result = FindRoots({35.138, 16.436, 14.625, 56.751, 32.496, 36.947, -40.991});
        CheckResult(expected, result);
    }
    {
        std::vector<double> expected{-3.676385, 2.594813};
        auto result = FindRoots({-10.609, -22.622, 85.254, 98.991, 18.077, 13.006, 149.512});
        CheckResult(expected, result);
    }
    {
        std::vector<double> expected{-1.812940, 0.104116};
        auto result = FindRoots({92.536, 144.455, -40.416, 55.347, 113.322, 31.945, -4.614});
        CheckResult(expected, result);
    }
    {
        std::vector<double> expected{-2.513753, -0.764358, 1.369307, 11.675366};
        auto result = FindRoots({2.286, -23.291, -45.569, 68.663, -1.500, 20.256, 56.044});
        CheckResult(expected, result);
    }
    {
        std::vector<double> expected{-1.121286, -0.003829};
        auto result = FindRoots({91.049, 40.596, -43.116, 25.294, 127.701, 148.319, 0.566});
        CheckResult(expected, result);
    }
    {
        std::vector<double> expected{-1.939891, -0.079616};
        auto result = FindRoots({18.517, 82.328, 145.290, 80.810, -18.256, 66.746, 5.465});
        CheckResult(expected, result);
    }
    {
        std::vector<double> expected{-1.144356, 0.157477};
        auto result = FindRoots({129.958, 21.278, 29.728, 79.603, -47.001, 93.000, -13.813});
        CheckResult(expected, result);
    }
    {
        std::vector<double> expected{-1.161232, -0.611987};
        auto result = FindRoots({145.405, 61.858, 19.482, 140.367, -19.155, 91.029, 89.994});
        CheckResult(expected, result);
    }
    {
        std::vector<double> expected{-4.116913, 0.082061};
        auto result = FindRoots({28.907, 132.979, 54.841, -17.429, -12.039, 57.844, -4.659});
        CheckResult(expected, result);
    }
    {
        std::vector<double> expected{};
        auto result = FindRoots({70.477, 5.254, 47.505, 89.167, 23.242, 112.646, 97.602});
        CheckResult(expected, result);
    }

}

int64_t GetSum(const std::vector<int>& roots, int mult_count, size_t cur_root,
               int cur_mult_count, int64_t cur_product) {
    if (mult_count == cur_mult_count)
        return cur_product;
    if (cur_root == roots.size())
        return 0;
    return GetSum(roots, mult_count, cur_root + 1, cur_mult_count, cur_product) +
           GetSum(roots, mult_count, cur_root + 1, cur_mult_count + 1, cur_product * roots[cur_root]);
}

void ManyIterations() {
    RandomGenerator rnd(76375754);
    for (int it = 0; it < 100; ++it) {
        std::vector<int> roots = rnd.GenIntegralVector(6, -7, 7);
        std::sort(roots.begin(), roots.end());
        std::vector<double> double_roots(roots.size());
        for (size_t i = 0; i < roots.size(); ++i) {
            double_roots[i] = roots[i];
        }

        std::vector<double> coeffs{1.0};
        for (int i = 1; i <= 6; ++i) {
            int sign = i % 2 == 1 ? -1 : 1;
            coeffs.push_back(sign * GetSum(roots, i, 0u, 0, 1));
        }

        auto result = FindRoots(coeffs);
        CheckResult(double_roots, result);
    }
}

void TestAll() {
    StartTesting();
    RUN_TEST(Simple);
    RUN_TEST(SomeTests);
    RUN_TEST(AnotherTests);
    RUN_TEST(ManyIterations);
}
} // namespace tests

int main() {
    std::cerr << std::fixed << std::setprecision(7);
    tests::TestAll();
    return 0;
}
