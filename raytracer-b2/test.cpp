#include <catch.hpp>

#include <cmath>
#include <string>
#include <optional>

#include <camera_options.h>
#include <render_options.h>
#include <commons.hpp>
#include <raytracer.h>

void CheckImage(const std::string& obj_filename, const std::string& result_filename,
                const CameraOptions& camera_options, const RenderOptions& render_options,
                std::optional<std::string> output_filename = std::nullopt) {
    auto image = Render(kBasePath + "tests/" + obj_filename, camera_options, render_options);
    if (output_filename.has_value()) {
        image.Write(output_filename.value());
    }
    Image ok_image(kBasePath + "tests/" + result_filename);
    Compare(image, ok_image);
}

TEST_CASE("Dragon", "[raytracer]") {
    CameraOptions camera_opts(1024, 768);
    camera_opts.fov = M_PI / 4;
    camera_opts.look_from = std::array<double, 3>{-1.1, 0.6, -0.5};
    camera_opts.look_to = std::array<double, 3>{0.20702, -0.26424, 0.214467};
    RenderOptions render_opts{8};
    CheckImage("dragon/dragon.obj", "dragon/result.png", camera_opts, render_opts);
}

TEST_CASE("Bunny", "[raytracer]") {
    CameraOptions camera_opts(1024, 768);
    camera_opts.look_from = std::array<double, 3>{-0.8, 1.0, 1.4};
    camera_opts.look_to = std::array<double, 3>{-0.2, 0.9, 0.0};
    RenderOptions render_opts{1};
    CheckImage("bunny/bunny.obj", "bunny/result.png", camera_opts, render_opts);
}

/* TEST_CASE("Figure", "[raytracer]") {
    CameraOptions camera_opts(1920, 1080);
    camera_opts.look_from = std::array<double, 3>{0, 1, 1.3};
    camera_opts.look_to = std::array<double, 3>{0, 1, -1};
    RenderOptions render_opts{8};
    CheckImage("sculpture/Kayle_sculpt1.OBJ", "sculpture/result.png",
               camera_opts, render_opts);
} */