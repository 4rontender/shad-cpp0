#include <catch.hpp>
#include <re2/re2.h>

#include <regexp.h>

#include <vector>
#include <string>

std::vector<std::string> GetTokens(const RE2& regexp, const std::string& string) {
    re2::StringPiece input(string);
    std::vector<std::string> result;
    std::string cur;
    while (RE2::FindAndConsume(&input, regexp, &cur)) {
        result.push_back(cur);
    }
    return result;
}

#define BRACED_INIT_LIST(...) {__VA_ARGS__}
#define RE_CHECK(S, E) do {\
    std::vector<std::string> expected(BRACED_INIT_LIST E);\
    REQUIRE(GetTokens(re, S) == expected);\
} while(false)

TEST_CASE("Tokens") {
    RE2 re(kTokenRegexp);
    REQUIRE(re.ok());
    RE_CHECK("123 + 256", ("123", "+", "256"));
    RE_CHECK("a * b - e+17", ("a", "*", "b", "-", "e", "+", "17"));
    RE_CHECK("Var___ + 5 = 7", ("Var___", "+", "5", "=", "7"));
    RE_CHECK("Var___etc + 5 = 7", ("Var___etc", "+", "5", "=", "7"));
    RE_CHECK("a_b__c + 5 = 7", ("a_b__c", "+", "5", "=", "7"));
    RE_CHECK("Var_17 += 7", ("Var_17", "+=", "7"));
    RE_CHECK("23*(1 + 3 -(-5))", ("23", "*", "(", "1", "+", "3", "-", "(", "-", "5", ")", ")"));
    RE_CHECK("not a var 17f", ("not", "a", "var", "17", "f"));
    RE_CHECK("a1*=17-=b=BIG", ("a1", "*=", "17", "-=", "b", "=", "BIG"));
    RE_CHECK("_ ?^", ());
}

TEST_CASE("Number") {
    RE2 re(kNumberRegexp);
    REQUIRE(re.ok());
    REQUIRE(RE2::FullMatch("0", re));
    REQUIRE(RE2::FullMatch("-123", re));
    REQUIRE(RE2::FullMatch("+17", re));
    REQUIRE(!RE2::FullMatch("-", re));
    REQUIRE(RE2::FullMatch("12.175", re));
    REQUIRE(RE2::FullMatch("+1.9", re));
    REQUIRE(RE2::FullMatch("-.179", re));
    REQUIRE(RE2::FullMatch(".14e+51", re));
    REQUIRE(RE2::FullMatch("0.7e7", re));
    REQUIRE(!RE2::FullMatch("15,5", re));
    REQUIRE(RE2::FullMatch("-7.19E  - 3", re));
    REQUIRE(!RE2::FullMatch("18.e2", re));
    REQUIRE(!RE2::FullMatch("17.", re));
    REQUIRE(RE2::FullMatch("1e5", re));
    REQUIRE(RE2::FullMatch("10e- 9", re));
    REQUIRE(!RE2::FullMatch("13.4e+", re));
    REQUIRE(!RE2::FullMatch("e5", re));
    REQUIRE(!RE2::FullMatch("1e9 + 12", re));
    REQUIRE(!RE2::FullMatch("-0.007 + 35", re));
    REQUIRE(!RE2::FullMatch(".", re));
    REQUIRE(RE2::FullMatch("-1e - 71", re));
    REQUIRE(!RE2::FullMatch("-1e2-71", re));

    std::string dummy;
    REQUIRE(!RE2::FullMatch("1.4e-17", re, &dummy));
}
